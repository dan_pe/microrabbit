Write-Host "*** Running DevSetup ***"

Get-ChildItem "C:\Repositories\microrabbit\MicroRabbit\scripts\*.ps1" | 
ForEach-Object {
  $filename = $_.FullName
  & $filename
}